# Hacknight workshop

Hello and welcome to the workshop. You just learned what a monorepo is and what Nx can do for you. Look at the 'Things to try out' below and see how things work or just hack away.

For detailed explainations about Nx concepts and more information, visit: https://nx.dev/

## Things to try out

Before you start, run `npm install` in the root of the repository.

1. OPTIONAL: Run the `npx create-nx-workspace` command yourself and see what you can make.
2. Continue with this project for the rest of the things.
3. Add a library to the libs folder and use it in the app. Run `npx nx graph` and see your library in the graph.
4. Run `npx nx run-many --target=test` and watch how Nx runs it for 3 apps and libs at the same time. Change the number of parallel tasks by adding `--parallel=NUM_OF_PARALLEL_TASKS` to the previous command.
5. Make a change in the project and run the `npx nx affected:test|build|lint` command to see that only the apps and libs that affect your change will be used.
6. Run `npx nx affected:graph` to see your change in the graph.
7. Open the VSCode extension `Nx Console` and you'll see all the commands you can run. Choose `generate` and look at all the things you can generate. Type _component_ and a form will open with all the available options and flags that you can tweak to make it just the way you like it.
8. Explore the other generators.
9. Explore all the other commands.

## End of the workshop

It may be the end of the workshop, but it may very well be the beginning of an adventure in Nx. Keep on hacking if you like it!
