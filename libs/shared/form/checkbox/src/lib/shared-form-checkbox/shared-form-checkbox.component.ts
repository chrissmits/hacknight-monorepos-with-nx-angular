import { CommonModule } from '@angular/common';
import { Component, forwardRef, Input, OnDestroy } from '@angular/core';
import {
  FormControl,
  NG_VALUE_ACCESSOR,
  ReactiveFormsModule,
} from '@angular/forms';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'hacknight-checkbox',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  templateUrl: './shared-form-checkbox.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => SharedFormCheckboxComponent),
    },
  ],
})
export class SharedFormCheckboxComponent implements OnDestroy {
  protected readonly destroy$ = new Subject<void>();

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  onTouch: any = new Function();

  isDisabled = false;

  readonly input = new FormControl();

  @Input() label = '';

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  writeValue(value: string): void {
    this.input.setValue(value);
  }

  registerOnChange(fn: never): void {
    this.input.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(fn);
  }

  registerOnTouched(fn: never): void {
    this.onTouch = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }
}
