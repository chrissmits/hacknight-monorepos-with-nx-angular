import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Todo } from '../models/todo';

@Injectable({
  providedIn: 'root',
})
export class TodoApiService {
  defaultTodos(): Observable<Todo[]> {
    return of([
      { todo: 'Prepare hacknight', checked: true },
      { todo: 'Presentation', checked: false },
      { todo: 'Workshop', checked: false },
    ]);
  }
}
