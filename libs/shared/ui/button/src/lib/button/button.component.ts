import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'hacknight-button',
  standalone: true,
  imports: [CommonModule],
  template: `<button
    class="border-2 p-3 rounded-lg hover:text-white"
    [ngClass]="{
      'border-green-500 text-green-500 hover:bg-green-500': color === 'primary',
      'border-slate-500 text-slate-500 hover:bg-slate-500':
        color === 'secondary',
      'border-red-500 text-red-500 hover:bg-red-500': color === 'danger'
    }"
    (click)="actionCallback.emit()"
  >
    <ng-content></ng-content>
  </button>`,
  styles: [],
})
export class ButtonComponent {
  @Input() color: 'primary' | 'secondary' | 'danger' = 'primary';

  @Output() actionCallback = new EventEmitter<void>();
}
