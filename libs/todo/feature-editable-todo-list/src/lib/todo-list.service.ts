import { Injectable } from '@angular/core';
import { Todo, TodoApiService } from '@hacknight/shared/api';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TodoListService {
  constructor(private readonly todoApiService: TodoApiService) {}

  get defaultTodos$(): Observable<Todo[]> {
    return this.todoApiService.defaultTodos();
  }
}
