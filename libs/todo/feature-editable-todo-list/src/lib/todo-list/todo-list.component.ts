import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import {
  FormArray,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
} from '@angular/forms';
import { Todo } from '@hacknight/shared/api';
import { SharedFormCheckboxComponent } from '@hacknight/shared/form/checkbox';
import { SharedFormInputComponent } from '@hacknight/shared/form/input';
import { ButtonComponent } from '@hacknight/shared/ui/button';
import { first } from 'rxjs';
import { TodoListService } from '../todo-list.service';

@Component({
  selector: 'hacknight-todo-list',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ButtonComponent,
    SharedFormInputComponent,
    SharedFormCheckboxComponent,
  ],
  templateUrl: './todo-list.component.html',
})
export class TodoListComponent implements OnInit {
  form = new FormGroup({
    todos: new FormArray<
      FormGroup<{
        todo: FormControl<string>;
        checked: FormControl<boolean>;
      }>
    >([]),
  });

  get todos(): FormArray {
    return this.form.controls.todos;
  }

  constructor(private readonly todoListService: TodoListService) {}

  ngOnInit(): void {
    this.todoListService.defaultTodos$
      .pipe(first())
      .subscribe((todos: Todo[]) => {
        todos.forEach(() => this.addTodo());
        this.todos.patchValue(todos);
      });
  }

  addTodo() {
    this.todos.push(
      new FormGroup({
        todo: new FormControl<string>('', { nonNullable: true }),
        checked: new FormControl<boolean>(false, { nonNullable: true }),
      })
    );
  }

  removeTodo(i: number) {
    this.todos.removeAt(i);
  }
}
