/** @type {import('tailwindcss').Config} */
module.exports = {
  theme: {},
  plugins: [
    require('@tailwindcss/forms')({
      theme: 'simple',
    }),
  ],
};
